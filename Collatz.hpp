// ---------
// Collatz.h
// ---------

#ifndef Collatz_h
#define Collatz_h

// --------
// includes
// --------

#include <iostream> // istream, ostream
#include <iterator> // istream_iterator
#include <tuple>    // tuple
#include <utility>  // pair

using namespace std;

// ------------
// collatz_read
// ------------

/**
 * read two uint64_ts
 * @param an istream_iterator
 * @return a pair of uint64_ts
 */
pair<uint64_t, uint64_t> collatz_read (istream_iterator<uint64_t>&);

// ------------
// collatz_eval
// ------------

/**
 * @param a pair of uint64_ts
 * @return a tuple of three uint64_ts
 */
tuple<uint64_t, uint64_t, uint64_t> collatz_eval (const pair<uint64_t, uint64_t>&);

// -------------
// collatz_pruint64_t
// -------------

/**
 * pruint64_t three uint64_ts
 * @param an ostream
 * @param a tuple of three uint64_ts
 */
void collatz_pruint64_t (ostream&, const tuple<uint64_t, uint64_t, uint64_t>&);

// -------------
// collatz_solve
// -------------

/**
 * @param an istream
 * @param an ostream
 */
void collatz_solve (istream&, ostream&);

// -------------
// collatz_pruint64_t
// -------------

#endif // Collatz_h
