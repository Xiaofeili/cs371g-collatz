// ---------------
// TestCollatz.c++
// ---------------

// https://code.google.com/p/googletest/wiki/V1_7_Primer#Basic_Assertions

// --------
// includes
// --------

#include <iostream> // cout, endl, istream
#include <iterator> // istream_iterator
#include <sstream>  // istringtstream, ostringstream
#include <tuple>    // make_tuple, tie, tuple
#include <utility>  // make_pair, pair

#include "gtest/gtest.h"

#include "Collatz.hpp"

using namespace std;

// -----------
// TestCollatz
// -----------

// ----
// read
// ----

TEST(CollatzFixture, read) {
    istringstream         iss("1 10\n");
    istream_iterator<uint64_t> begin_iterator(iss);
    pair<uint64_t, uint64_t> p = collatz_read(begin_iterator);
    uint64_t i;
    uint64_t j;
    tie(i, j) = p;
    ASSERT_EQ(i,  1);
    ASSERT_EQ(j, 10);
}

// ----
// eval
// ----

TEST(CollatzFixture, eval0) {
    tuple<uint64_t, uint64_t, uint64_t> t = collatz_eval(make_pair(1, 10));
    uint64_t i;
    uint64_t j;
    uint64_t v;
    tie(i, j, v) = t;
    ASSERT_EQ(i,  1);
    ASSERT_EQ(j, 10);
    ASSERT_EQ(v, 20);
}

TEST(CollatzFixture, eval1) {
    tuple<uint64_t, uint64_t, uint64_t> t = collatz_eval(make_pair(100, 200));
    uint64_t i;
    uint64_t j;
    uint64_t v;
    tie(i, j, v) = t;
    ASSERT_EQ(i, 100);
    ASSERT_EQ(j, 200);
    ASSERT_EQ(v, 125);
}

TEST(CollatzFixture, eval2) {
    tuple<uint64_t, uint64_t, uint64_t> t = collatz_eval(make_pair(201, 210));
    uint64_t i;
    uint64_t j;
    uint64_t v;
    tie(i, j, v) = t;
    ASSERT_EQ(i, 201);
    ASSERT_EQ(j, 210);
    ASSERT_EQ(v, 89);
}

TEST(CollatzFixture, eval3) {
    tuple<uint64_t, uint64_t, uint64_t> t = collatz_eval(make_pair(900, 1000));
    uint64_t i;
    uint64_t j;
    uint64_t v;
    tie(i, j, v) = t;
    ASSERT_EQ(i,  900);
    ASSERT_EQ(j, 1000);
    ASSERT_EQ(v, 174);
}

TEST(CollatzFixture, eval4) {
    tuple<uint64_t, uint64_t, uint64_t> t = collatz_eval(make_pair(763068, 762818));
    uint64_t i;
    uint64_t j;
    uint64_t v;
    tie(i, j, v) = t;
    ASSERT_EQ(i,  763068);
    ASSERT_EQ(j, 762818);
    ASSERT_EQ(v, 269);
}

TEST(CollatzFixture, eval5) {
    tuple<uint64_t, uint64_t, uint64_t> t = collatz_eval(make_pair(1, 1));
    uint64_t i;
    uint64_t j;
    uint64_t v;
    tie(i, j, v) = t;
    ASSERT_EQ(i,  1);
    ASSERT_EQ(j, 1);
    ASSERT_EQ(v, 1);
}

TEST(CollatzFixture, eval6) {
    tuple<uint64_t, uint64_t, uint64_t> t = collatz_eval(make_pair(10, 10));
    uint64_t i;
    uint64_t j;
    uint64_t v;
    tie(i, j, v) = t;
    ASSERT_EQ(i,  10);
    ASSERT_EQ(j, 10);
    ASSERT_EQ(v, 7);
}

TEST(CollatzFixture, eval7) {
    tuple<uint64_t, uint64_t, uint64_t> t = collatz_eval(make_pair(10, 1));
    uint64_t i;
    uint64_t j;
    uint64_t v;
    tie(i, j, v) = t;
    ASSERT_EQ(i,  10);
    ASSERT_EQ(j, 1);
    ASSERT_EQ(v, 20);
}

TEST(CollatzFixture, eval8) {
    tuple<uint64_t, uint64_t, uint64_t> t = collatz_eval(make_pair(8, 10));
    uint64_t i;
    uint64_t j;
    uint64_t v;
    tie(i, j, v) = t;
    ASSERT_EQ(i,  8);
    ASSERT_EQ(j, 10);
    ASSERT_EQ(v, 20);
}

// -----
// pruint64_t
// -----

TEST(CollatzFixture, pruint64_t) {
    ostringstream oss;
    collatz_pruint64_t(oss, make_tuple(1, 10, 20));
    ASSERT_EQ(oss.str(), "1 10 20\n");
}

// -----
// solve
// -----

TEST(CollatzFixture, solve) {
    istringstream iss("1 10\n100 200\n201 210\n900 1000\n");
    ostringstream oss;
    collatz_solve(iss, oss);
    ASSERT_EQ("1 10 20\n100 200 125\n201 210 89\n900 1000 174\n", oss.str());
}