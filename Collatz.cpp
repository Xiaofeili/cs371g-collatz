// -----------
// Collatz.c++
// -----------

// --------
// includes
// --------

#include <cassert>  // assert
#include <iostream> // endl, istream, ostream
#include <iterator> // istream_iterator
#include <tuple>    // make_tuple, tie, tuple
#include <utility>  // make_pair, pair

#include "Collatz.hpp"

using namespace std;

const uint64_t SIZE = 1000000;
uint64_t a[SIZE]= {};
bool pre = false;


// ------------
// collatz_read
// ------------

pair<uint64_t, uint64_t> collatz_read (istream_iterator<uint64_t>& begin_iterator) {
    uint64_t i = *begin_iterator;
    ++begin_iterator;
    uint64_t j = *begin_iterator;
    ++begin_iterator;
    return make_pair(i, j);
}
// ------------
// collatz_length
// ------------

uint64_t collatz_length (uint64_t n) {
    assert(n > 0);
    uint64_t result = 1;
    if(n < SIZE) {
        if(a[n] > 0)
            return a[n];
    }
    while(n > 1) {
        if ((n % 2) == 0) {
            n = n/2;
            if(n < SIZE) {
                if(a[n] > 0) {
                    result += a[n];
                    break;
                }
            }
            ++result;
        }
        else {
            n = (n + (n >> 1) + 1);
            if(n < SIZE) {
                if(a[n] > 0) {
                    result += a[n];
                    break;
                }
            }
            ++++result;
        }
    }
    assert(result > 0);
    return result;
}

// ------------
// collatz_setup
// ------------

void collatz_setup () {
    for(uint64_t i = 1; i < SIZE; ++i)
        a[i] = collatz_length(i);
}



// ------------
// collatz_eval
// ------------

tuple<uint64_t, uint64_t, uint64_t> collatz_eval (const pair<uint64_t, uint64_t>& p) {
    if(!pre) {
        collatz_setup();
        pre = true;
    }
    uint64_t i;
    uint64_t j;
    uint64_t c = 0;
    uint64_t k = 1;
    tie(i, j) = p;
    if(i > j) {
        c = i;
        i = j;
        j = c;
    }
    uint64_t m = j/2 + 1;
    m = (i < m)? m : i;
    for (uint64_t n = m; n <= j ; ++n ) {
        uint64_t tmp = collatz_length(n);
        k = (tmp > k)? tmp: k;
    }
    if(c != 0) {
        c = i;
        i = j;
        j = c;
    }
    return make_tuple(i, j, k);
}



// -------------
// collatz_pruint64_t
// -------------

void collatz_pruint64_t (ostream& sout, const tuple<uint64_t, uint64_t, uint64_t>& t) {
    uint64_t i;
    uint64_t j;
    uint64_t v;
    tie(i, j, v) = t;
    sout << i << " " << j << " " << v << endl;
}



// -------------
// collatz_solve
// -------------

void collatz_solve (istream& sin, ostream& sout) {
    istream_iterator<uint64_t> begin_iterator(sin);
    istream_iterator<uint64_t> end_iterator;
    while (begin_iterator != end_iterator) {
        collatz_pruint64_t(sout, collatz_eval(collatz_read(begin_iterator)));
    }
}




